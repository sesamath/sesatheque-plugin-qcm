import PropTypes from 'prop-types'
import React from 'react'
import { hot } from 'react-hot-loader'
import { Provider } from 'react-redux'
import QCM from './components/QCM'

import './App.scss'

const App = ({ store }) => (
  <Provider store={store}>
    <div className="qcm-wrapper">
      <QCM />
    </div>
  </Provider>
)

App.propTypes = {
  store: PropTypes.object.isRequired
}

export default hot(module)(App)
