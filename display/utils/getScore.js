import { localStoreGet } from '../localStore'

const getScore = (reponses) => {
  const stats = {
    incomplete: 0,
    error: 0,
    complete: 0
  }
  const scoreObj = {
    score: null,
    stats
  }

  if (reponses.length === 0) return scoreObj
  const { correction, penalite } = localStoreGet('parametres')

  scoreObj.score = reponses.reduce((score, reponse, index) => {
    const rightAnswer = new Set(correction[index].validIndices)
    if (rightAnswer.size === 0) return score

    const filteredAnswer = reponse.filter(item => (item !== null))

    let questionScore = 0
    const hasOnlyValidAnswers = filteredAnswer.every(item => {
      if (rightAnswer.has(item)) {
        questionScore += 1
        return true
      }

      return false
    })

    if (hasOnlyValidAnswers) {
      if (questionScore === rightAnswer.size) {
        stats.complete++
      } else {
        stats.incomplete++
      }
      return score + questionScore / rightAnswer.size
    }
    stats.error++

    return score - penalite
  }, 0)

  return scoreObj
}

export default getScore
