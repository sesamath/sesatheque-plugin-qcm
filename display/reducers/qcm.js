const qcmReducer = (state, { type, reponse }) => {
  switch (type) {
    case 'PUSH_ANSWER':
      return {
        ...state,
        reponses: [...state.reponses, reponse]
      }
    case 'GO_FORWARD':
      return {
        ...state,
        questionIndex: state.questionIndex + 1
      }
    case 'GO_BACKWARD':
      return {
        ...state,
        questionIndex: state.questionIndex - 1
      }
    default:
      return state
  }
}

export default qcmReducer
