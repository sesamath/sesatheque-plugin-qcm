import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import Home from './Home'
import Question from './Question'
import Bilan from './Bilan'
import { localStoreGet } from '../localStore'

const QCM = ({ questionIndex }) => {
  if (questionIndex < 0) return (<Home />)
  if (questionIndex >= localStoreGet('parametres').questions.length) return (<Bilan />)

  return (<Question
    questionIndex={questionIndex}
  />)
}

QCM.propTypes = {
  questionIndex: PropTypes.number
}

export default connect(({
  questionIndex
}) => ({
  questionIndex
}), {})(QCM)
