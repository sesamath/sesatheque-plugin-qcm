import PropTypes from 'prop-types'
import React from 'react'
import MathJax from './MathJax'

const Choice = ({
  className,
  disabled,
  checked,
  onChange,
  content
}) => (
  <li>
    <label className={className}>
      <input
        disabled={disabled}
        type="checkbox"
        checked={checked}
        onChange={onChange}
      />
      <MathJax
        className="mathjax-choice"
        math={content}
      />
    </label>
  </li>
)

Choice.propTypes = {
  className: PropTypes.string,
  disabled: PropTypes.bool.isRequired,
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  content: PropTypes.string
}

export default Choice
