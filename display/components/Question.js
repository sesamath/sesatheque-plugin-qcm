import PropTypes from 'prop-types'
import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { pushAnswer } from '../actions/qcm'
import { localStoreGet } from '../localStore'
import Choice from './Choice'
import Nav from './Nav'
import Score from './Score'
import MathJax from './MathJax'

class Question extends Component {
  constructor (props) {
    super(props)
    this.state = {
      checked: this.props.reponse || []
    }
  }

  updateState (index) {
    const { checked } = this.state
    if (checked.includes(index)) {
      this.setState({
        checked: checked.filter(item => (item !== index))
      })
    } else {
      if (index === null) return this.setState({ checked: [null] })

      this.setState({
        checked: [...checked.filter(item => (item !== null)), index]
      })
    }
  }

  getClass (index) {
    const correction = this.props.correction
    if (correction === undefined) return ''

    const isChecked = this.state.checked.includes(index)
    const isCorrect = correction.validIndices.includes(index)
    if (isChecked) {
      if (isCorrect) return 'green-bg'
      else return 'red-bg'
    } else {
      if (isCorrect) return 'orange-bg'
      else return ''
    }
  }

  componentDidUpdate (prevProps) {
    if (this.props.questionIndex !== prevProps.questionIndex) {
      this.setState({ checked: this.props.reponse || [] })
    }
  }

  render () {
    const {
      question: { enonce, choix },
      correction,
      questionIndex,
      questionsLength,
      pushAnswer
    } = this.props

    const navProps = correction
      ? {}
      : {
          nextDisabled: this.state.checked.length === 0,
          nextListener: () => {
            pushAnswer(this.state.checked)
          },
          nextContent: 'Valider'
        }

    return (
      <Fragment>
        <Nav {...navProps} />
        <Score />
        <h2 className="enonce">
          <span className="questionIndex">
            {questionIndex + 1}.
          </span>
          <MathJax
            math={enonce}
          />
        </h2>
        <ul className="choices">
          {choix.map((content, index) => (
            <Choice
              key={index}
              className={this.getClass(index)}
              disabled={correction !== undefined}
              checked={this.state.checked.includes(index)}
              onChange={() => this.updateState(index)}
              content={content}
            />
          ))}
          {localStoreGet('parametres').addDoNotKnow
            ? (
            <Choice
              disabled={correction !== undefined}
              checked={this.state.checked.includes(null)}
              onChange={() => this.updateState(null)}
              content="Je ne sais pas"
            />
              )
            : null}
        </ul>
        {correction && correction.explication
          ? (
          <MathJax
            math={correction.explication}
          />
            )
          : null}
        <div className="pagination">
          {questionIndex + 1} / {questionsLength}
        </div>
      </Fragment>
    )
  }
}

Question.propTypes = {
  reponse: PropTypes.arrayOf(PropTypes.number),
  question: PropTypes.object,
  correction: PropTypes.shape({
    validIndices: PropTypes.arrayOf(PropTypes.number).isRequired,
    explication: PropTypes.string
  }),
  pushAnswer: PropTypes.func.isRequired,
  questionsLength: PropTypes.number,
  questionIndex: PropTypes.number
}

const mapStateToProps = ({
  reponses
}, { questionIndex }) => ({
  questionsLength: localStoreGet('parametres').questions.length,
  reponse: reponses[questionIndex],
  question: localStoreGet('parametres').questions[questionIndex],
  correction: reponses[questionIndex] && localStoreGet('parametres').correction[questionIndex]
})

export default connect(mapStateToProps, { pushAnswer })(
  Question
)
