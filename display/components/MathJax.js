import React, { Component } from 'react'
import PropTypes from 'prop-types'
import loadjs from 'loadjs'

// on prend la même url que mathgraph & instrumenpoche & j3p, c'est probablement déjà en cache
const mathjaxUrl = 'https://www.mathgraph32.org/js/MathJax3/es5/tex-svg.js'

class MathJaxRender extends Component {
  constructor (props) {
    super(props)
    this.previewRef = React.createRef()
  }

  renderMath () {
    if (!window.MathJax || typeof window.MathJax.typesetPromise !== 'function') return console.error(Error('MathJax n’est pas correctement chargé'))
    // on retourne une promesse et on laisse React gérer l'erreur éventuelle
    window.MathJax.typesetPromise([this.previewRef.current]).catch(console.error.bind(console))
  }

  componentDidMount () {
    this.previewRef.current.innerHTML = this.props.math
    if (!loadjs.isDefined('mathjax')) {
      // cf https://mathjax.github.io/MathJax-demos-web/convert-configuration/convert-configuration.html pour convertir une config v2 => v3
      window.MathJax = {
        tex: {
          inlineMath: [['$', '$'], ['\\(', '\\)']]
        }
      }
      return loadjs(mathjaxUrl, 'mathjax', this.renderMath.bind(this))
    }
    // on appelle pas renderMath d'office, au cas où le chargement aurait déjà démarré (lors d'un montage précédent de ce component) mais ne serait pas terminé
    loadjs.ready('mathjax', this.renderMath.bind(this))
  }

  shouldComponentUpdate (nextProps) {
    return nextProps.math !== this.props.math
  }

  componentDidUpdate () {
    this.previewRef.current.innerHTML = this.props.math
    loadjs.ready('mathjax', this.renderMath.bind(this))
  }

  render () {
    return (
      <div
        className={this.props.className}
        style={this.props.style}
      >
        <div
          ref={this.previewRef}
        >
        </div>
      </div>
    )
  }
}

MathJaxRender.propTypes = {
  className: PropTypes.string,
  style: PropTypes.string,
  math: PropTypes.string
}

MathJaxRender.defaultProps = {
  className: 'mathjax-renderer-container',
  math: ''
}

export default MathJaxRender
