import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import getScore from '../utils/getScore'

const Score = ({ reponses }) => {
  const { score } = getScore(reponses)
  if (score === null) return null

  return (
    <div className="score">Score: {score}</div>
  )
}

Score.propTypes = {
  reponses: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number))
}

const mapStateToProps = ({ reponses }) => ({ reponses })

export default connect(mapStateToProps, {})(Score)
