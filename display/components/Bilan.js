import PropTypes from 'prop-types'
import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import getScore from '../utils/getScore'
import { localStoreGet } from '../localStore'
import Nav from './Nav'

const Stat = ({
  total,
  singleContent,
  manyContent
}) => {
  if (total > 1) return (<li>{total} {manyContent}</li>)
  if (total === 1) return (<li>{total} {singleContent}</li>)

  return null
}

Stat.propTypes = {
  total: PropTypes.number.isRequired,
  singleContent: PropTypes.string.isRequired,
  manyContent: PropTypes.string.isRequired
}

const Bilan = ({ reponses }) => {
  const {
    score,
    stats: {
      error,
      complete,
      incomplete
    }
  } = getScore(reponses)
  const normalizedScore = Math.max(score, 0)
  const nQuestions = localStoreGet('parametres').questions.length

  return (
    <Fragment>
      <Nav nextDisabled />
      <p>Vous avez répondu à toutes les questions de ce QCM. Vous avez obtenu:</p>
      <ul>
        <Stat
          total={complete}
          singleContent={'bonne réponse'}
          manyContent={'bonnes réponses'}
        />
        <Stat
          total={incomplete}
          singleContent={'réponse incomplète'}
          manyContent={'réponses incomplètes'}
        />
        <Stat
          total={error}
          singleContent={'mauvaise réponse'}
          manyContent={'mauvaises réponses'}
        />
      </ul>
      <p> Votre score normalisé: {Math.round(100 * normalizedScore / nQuestions)}%.</p>
    </Fragment>
  )
}

Bilan.propTypes = {
  reponses: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number))
}

const mapStateToProps = ({ reponses }) => ({ reponses })

export default connect(mapStateToProps, {})(
  Bilan
)
