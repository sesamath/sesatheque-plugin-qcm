import React, { Fragment } from 'react'
import Nav from './Nav'
import Score from './Score'
import { localStoreGet } from '../localStore'

const Home = () => {
  const { addDoNotKnow, penalite } = localStoreGet('parametres')

  return (
    <Fragment>
      <Nav backDisabled />
      <Score />
      <p>
        Bienvenu sur ce QCM Sesamath.
      </p>
      <p>
        Chaque question comporte au moins une réponse correcte et peut rapporter un point. Une réponse validée ne peut plus être modifiée. Une fraction de réponses correctes rapporte une fraction des points.
      </p>
      {penalite > 0
        ? (
        <p>
          Attention, une question comprenant au moins une réponse incorrecte ne rapporte aucun point et réduit le score de {penalite} points.
        </p>
          )
        : (
        <p>
          Une question comprenant au moins une réponse incorrecte n’apporte aucun point.
        </p>
          )}
      {addDoNotKnow
        ? (
        <p>Choisir la réponse &quot;Je ne sais pas&quot; ne rapporte aucun point et n’entraîne aucune pénalité.</p>
          )
        : null}
    </Fragment>
  )
}

export default Home
