import PropTypes from 'prop-types'
import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import { goNext, goBack } from '../actions/qcm'

const Nav = ({
  backDisabled = false,
  backListener,
  nextContent = 'Suivant',
  nextDisabled = false,
  nextListener
}) => (
  <Fragment>
    <button
      type="button"
      onClick={backListener}
      disabled={backDisabled}
    >
      Retour
    </button>
    <button
      type="button"
      onClick={nextListener}
      disabled={nextDisabled}
    >
      {nextContent}
    </button>
  </Fragment>
)

Nav.propTypes = {
  backDisabled: PropTypes.bool,
  backListener: PropTypes.func.isRequired,
  nextContent: PropTypes.string,
  nextDisabled: PropTypes.bool,
  nextListener: PropTypes.func.isRequired
}

const mapDispatchToProps = (dispatch, {
  nextListener
}) => ({
  backListener: () => dispatch(goBack()),
  nextListener: nextListener || (() => dispatch(goNext()))
})

export default connect(null, mapDispatchToProps)(Nav)
