import { applyMiddleware, compose, createStore } from 'redux'
import thunkMiddleware from 'redux-thunk'
import qcm from './reducers/qcm'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const defaultInit = {
  reponses: [],
  questionIndex: -1
}

const buildStore = (init = defaultInit) => createStore(
  qcm,
  init,
  composeEnhancers(applyMiddleware(
    thunkMiddleware
  ))
)

export default buildStore
