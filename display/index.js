import { createElement } from 'react'
import { render } from 'react-dom'
import sjtUrl from 'sesajstools/http/url'
import xhr from 'sesajstools/http/xhr'
import App from './App'
import buildStore from './buildStore'
import { localStoreSet } from './localStore'
import { addError } from 'client/page'

const fetchLastResult = (load) => {
  const lastResultUrl = sjtUrl.getParameter('lastResultUrl')
  if (lastResultUrl) {
    xhr.get(lastResultUrl, { responseType: 'json' }, function (error, data) {
      if (error) {
        console.error(error)
        addError('Impossible de récupérer le dernier résultat')
      } else if (data) {
        if (data.success) {
          if (data.resultat) {
            const { contenu } = data.resultat
            return load(contenu)
          }
        } else {
          console.error("l'appel de lastResultat a échoué", data)
        }
      }
      load()
    })
  } else {
    load()
  }
}

export const display = ({
  parametres
}, {
  container,
  resultatCallback
}, next) => {
  localStoreSet('parametres', parametres)
  localStoreSet('resultatCallback', resultatCallback)
  fetchLastResult((initialValues) => {
    document.body.classList.add('qcm-display')
    render(createElement(App, { store: buildStore(initialValues) }), container)
    next()
  })
}
