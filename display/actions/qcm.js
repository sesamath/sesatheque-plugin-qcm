import { localStoreGet } from '../localStore'
import getScore from '../utils/getScore'

/**
 * Retourne l'action de type GO_BACKWARD pour revenir à l'étape précédente
 * @return {{type: string}}
 */
export const goBack = () => ({
  type: 'GO_BACKWARD'
})

/**
 * Retourne l'action de type GO_FORWARD pour aller à l'étape suivante
 * @return {{type: string}}
 */
export const goNext = () => ({
  type: 'GO_FORWARD'
})

export const pushAnswer = reponse => (dispatch, getState) => {
  dispatch({
    type: 'PUSH_ANSWER',
    reponse
  })

  const resultatCallback = localStoreGet('resultatCallback')
  if (resultatCallback) {
    const { questions } = localStoreGet('parametres')
    const nQuestions = questions.length
    const qcmState = getState()
    const { questionIndex, reponses } = qcmState
    const fin = (questionIndex === (nQuestions - 1))

    let score = 0
    if (fin) {
      score = Math.max(getScore(reponses).score, 0) / nQuestions
    }

    resultatCallback({
      score,
      fin,
      contenu: qcmState
    })
  }
}
