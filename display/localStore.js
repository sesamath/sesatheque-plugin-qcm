// un objet simple pour stocker des données qu'on ne veut pas mettre dans le store redux
const localStore = {}

export const localStoreGet = prop => localStore[prop]

export const localStoreSet = (prop, data) => {
  localStore[prop] = data
}
