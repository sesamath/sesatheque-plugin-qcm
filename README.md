# sesatheque-plugin-qcm

Un plugin de QCM pour la sésathèque.

Les fonctionnalités de base sont en en place.

Il est possible de saisir du texte enrichi de Latex dans les énoncés, les réponses et les corrigés mais il n'y a pas de prévisualisation du résultat dans l'éditeur pour l'instant. Côté élève, le rendu du texte enrichi par MathJax est fonctionnel.
