import editor from './EditorQCM'
import validate from './validate'
import type from '../type'

const defaultValue = {
  parametres: {
    penalite: 0,
    addDoNotKnow: false,
    questions: [],
    correction: []
  }
}

const loadHook = ({
  parametres: {
    questions,
    correction,
    ...otherParametres
  },
  ...others
}) => ({
  ...others,
  parametres: {
    ...otherParametres,
    questions: questions.map(({ choix, ...questionOthers }, questionIndex) => {
      const {
        validIndices,
        explication
      } = correction[questionIndex]

      return {
        ...questionOthers,
        choix: choix.map((reponse, reponseIndex) => {
          return {
            content: reponse,
            isValid: validIndices.includes(reponseIndex)
          }
        }),
        explication
      }
    })
  }
})

const saveHook = ({
  parametres: {
    questions,
    ...otherParametres
  },
  ...others
}) => {
  const trimmedQuestions = []
  const correction = questions.map(({
    choix,
    explication,
    ...questionOther
  }) => {
    const validIndices = []
    const trimmedReponses = []
    choix.forEach(({ isValid, content }, index) => {
      trimmedReponses.push(content)
      if (isValid) validIndices.push(index)
    })
    trimmedQuestions.push({
      ...questionOther,
      choix: trimmedReponses
    })
    return {
      validIndices,
      explication
    }
  })

  return {
    ...others,
    parametres: {
      ...otherParametres,
      questions: trimmedQuestions,
      correction
    }
  }
}

export { editor, type, validate, defaultValue, saveHook, loadHook }
