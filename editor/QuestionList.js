import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Container, Draggable } from 'react-smooth-dnd'
import Question from './Question'

class QuestionList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      showDetails: true
    }
  }

  onDrop ({ removedIndex, addedIndex }) {
    this.props.fields.move(removedIndex, addedIndex)
  }

  render () {
    const { fields } = this.props

    return (
      <div>
        <button
          type="button"
          className={`btn reponses-toggle ${this.state.showDetails ? '' : 'selected'}`}
          onClick={() => {
            const { showDetails } = this.state
            this.setState({
              showDetails: !showDetails
            })
          }}
        >
          Masquer les détails
        </button>
        <h2>Listes des questions</h2>
        <p>Vous pouvez saisir des symboles mathématiques avec du $code LaTeX$, par ex $\frac&#123;1&#125;&#123;2&#125;$ pour 1/2 (corrrectement affiché) ou $3^2$ pour 3² ou $\sqrt&#123;2&#125;$ pour une racine. <em>(mettre $$code LaTeX$$ pour que la formule soit affichée dans un bloc séparé)</em><br />
          Le rendu n’est pas encore possible sur cette page (après enregistrement, ouvrir l’aperçu dans un autre onglet pour vérifier).<br/>
          <a href="https://fr.wikibooks.org/wiki/LaTeX/Math%C3%A9matiques" target="_blank" rel="noopener noreferrer">Mémo</a>
        </p>
        <Container
          nonDragAreaSelector="input, textarea"
          onDrop={this.onDrop.bind(this)}
        >
          {fields.map((question, index) => (
            <Draggable key={index}>
              <Question
                index={index}
                showDetails={this.state.showDetails}
                fields={fields}
                question={question}
              />
            </Draggable>
          ))}
        </Container>
        <button
          className="addQuestion"
          type="button"
          onClick={() => fields.push({ choix: [] })}
        >
          Nouvelle question
        </button>
      </div>
    )
  }
}

QuestionList.propTypes = {
  fields: PropTypes.shape({
    map: PropTypes.func.isRequired,
    move: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired
  }).isRequired
}

export default QuestionList
