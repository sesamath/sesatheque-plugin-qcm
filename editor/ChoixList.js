import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Container, Draggable } from 'react-smooth-dnd'
import { InputField, SwitchField } from 'client-react/components/fields'
import showInvalidField from 'client-react/components/fields/hoc/showInvalidField'

class ChoixList extends Component {
  onDrop ({ removedIndex, addedIndex }) {
    this.props.fields.move(removedIndex, addedIndex)
  }

  render () {
    const { showDetails, fields } = this.props

    return (
      <div className="sesaqcm-reponse-list">
        {showDetails
          ? (
          <div>
            <Container
              nonDragAreaSelector="input"
              onDrop={this.onDrop.bind(this)}
            >
              {fields.map((answer, index) => (
                <Draggable key={index}>
                  <div className="sesaqcm-reponse">
                    <i
                      className="fa fa-lg fa-times sesaqcm-remove"
                      title="Supprimer la réponse"
                      onClick={() => fields.remove(index)}
                    />
                    <div className="grid-4">
                      <InputField
                        className="col-3"
                        name={`${answer}.content`}
                        type="text"
                        label={`Réponse #${index + 1}`}
                      />
                      <SwitchField
                        className="center"
                        label="Bonne réponse"
                        name={`${answer}.isValid`}
                      />
                    </div>
                  </div>
                </Draggable>
              ))}
            </Container>
            <button
              className="addReponse"
              type="button"
              onClick={() => fields.push({ isValid: false })}
            >
              Ajouter une réponse
            </button>
          </div>
            )
          : null}
      </div>
    )
  }
}

ChoixList.propTypes = {
  showDetails: PropTypes.bool,
  fields: PropTypes.shape({
    map: PropTypes.func.isRequired,
    move: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    remove: PropTypes.func.isRequired
  }).isRequired
}

export default showInvalidField(ChoixList, 'dirty')
