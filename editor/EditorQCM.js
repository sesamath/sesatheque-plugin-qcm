import React, { Component } from 'react'
import { FieldArray } from 'redux-form'
import Options from './Options'
import QuestionList from './QuestionList'

import './EditorQCM.scss'

/**
 * Éditeur des paramètres d'une ressource qcm
 */
class EditorQCM extends Component {
  render () {
    return (
      <fieldset className="qcm-editor">
        <Options />
        <hr />
        <FieldArray
          name="parametres[questions]"
          component={QuestionList}
        />
      </fieldset>
    )
  }
}

export default EditorQCM
