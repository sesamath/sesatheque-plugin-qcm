const validate = ({ parametres: { questions } }, errors) => {
  questions.forEach(({ choix }, index) => {
    if (!choix.some(({ isValid }) => isValid)) {
      errors.parametres = errors.parametres || {}
      errors.parametres.questions = errors.parametres.questions || []
      errors.parametres.questions[index] = {
        choix: { _error: 'Il faut au moins une réponse valide' }
      }
    }
  })
}

export default validate
