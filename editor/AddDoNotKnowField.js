import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Field, formValues } from 'redux-form'
import addLabel from 'client-react/components/fields/hoc/addLabel'
import Input from 'client-react/components/fields/inputs/Input'

class CustomCheckbox extends Component {
  componentDidUpdate () {
    const { hasPenalite, input: { value, onChange } } = this.props
    if (hasPenalite && !value) {
      onChange(true)
    }
  }

  render () {
    return (<Input {...this.props} />)
  }
}

CustomCheckbox.propTypes = {
  hasPenalite: PropTypes.bool.isRequired,
  input: PropTypes.shape({
    value: PropTypes.bool,
    onChange: PropTypes.func
  })
}

const AddDoNotKnowField = ({ penalite, name }) => {
  const hasPenalite = (penalite > 0)

  return (
    <Field
      disabled={hasPenalite}
      name={name}
      hasPenalite={hasPenalite}
      className="switch"
      component={CustomCheckbox}
      type="checkbox"
    />
  )
}

AddDoNotKnowField.propTypes = {
  penalite: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired
}

export default addLabel(formValues({ penalite: 'parametres[penalite]' })(AddDoNotKnowField))
