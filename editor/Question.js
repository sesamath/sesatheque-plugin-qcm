import PropTypes from 'prop-types'
import React from 'react'
import { FieldArray } from 'redux-form'
import { InputField, TextareaField } from 'client-react/components/fields'
import ChoixList from './ChoixList'

const Question = ({
  index,
  fields,
  question,
  showDetails
}) => (
  <div className="sesaqcm-question">
    <i
      className="fa fa-2x fa-times sesaqcm-remove"
      title="Supprimer la question"
      onClick={() => fields.remove(index)}
    />
    <InputField
      label={`Libellé de la question #${index + 1}`}
      name={`${question}.enonce`}
      type="text"
    />
    {showDetails
      ? (
      <TextareaField
        className="explication"
        label="Corrigé"
        name={`${question}.explication`}
        rows="2"
      />
        )
      : null}
    <FieldArray
      showDetails={showDetails}
      questionIndex={index}
      name={`${question}.choix`}
      component={ChoixList}
      question={question}
    />
  </div>
)

Question.propTypes = {
  index: PropTypes.number.isRequired,
  fields: PropTypes.shape({
    remove: PropTypes.func.isRequired
  }).isRequired,
  question: PropTypes.string.isRequired,
  showDetails: PropTypes.bool.isRequired
}

export default Question
