import React from 'react'
import { SelectField } from 'client-react/components/fields'
import AddDoNotKnowField from './AddDoNotKnowField'

const Options = () => (
  <div>
    <h2>Options</h2>
    <div className="grid-2">
      <SelectField
        label="Pénalité en cas de mauvaise réponse"
        name="parametres[penalite]"
        options={[
          { value: 0, label: '0' },
          { value: 0.25, label: '0,25' },
          { value: 0.5, label: '0,5' },
          { value: 1, label: '1' },
          { value: 2, label: '2' }
        ]}
      />
      <AddDoNotKnowField
        label={'Ajout automatique de la réponse "je ne sais pas"'}
        name="parametres[addDoNotKnow]"
      />
    </div>
  </div>
)

export default Options
