const path = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = () => ({
  entries: {},
  plugins: [
    new CopyWebpackPlugin({
      patterns: [{
        from: path.resolve(__dirname, 'public', 'qcm.gif'),
        to: 'plugins/qcm/[name][ext]'
      }]
    })
  ],
  rules: []
})
